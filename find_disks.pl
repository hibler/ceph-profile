#!/usr/bin/perl
#
# Print a comma seperated list of unused HDDs and another of unused SSDs
# for Ceph to use as it will.
#
# Let's not suffer any more than we have to and try to do this in bash.
#
use English;
use strict;
use Getopt::Std;

# Drag in path stuff so we can find emulab stuff.
BEGIN { require "/etc/emulab/paths.pm"; import emulabpaths; }

sub usage()
{
    print STDERR "Usage: find_disks [-SHA] [-s size]\n";
    print STDERR "Print a list of local unused disks of a particular type.\n";
    print STDERR "Output is one disk per line.\n";
    print STDERR "-h         This message\n";
    print STDERR "-A         Show all disks regardless of type\n";
    print STDERR "-H         Show all HDDs (default)\n";
    print STDERR "-S         Show all SSDs\n";
    print STDERR "-c         Show on one line, comma separated\n";
    print STDERR "-s size    Only show if size >= 'size' GiB (default=1)\n";
    exit(-1);
}

sub gather($$);

my $optlist  = "SHAcs:";
my $dohdd = 1;
my $dossd = 0;
my $docsv = 0;
my $minsize = 1;

#
# Parse command arguments. Once we return from getopts, all that should be
# left are the required arguments.
#
my %options = ();
if (! getopts($optlist, \%options)) {
    usage();
}
if ($options{'A'}) {
    $dohdd = $dossd = 1;
} elsif ($options{'H'}) {
    $dohdd = 1;
    $dossd = 0;
} elsif ($options{'S'}) {
    $dossd = 1;
    $dohdd = 0;
}
if ($options{'c'}) {
    $docsv = 1;
}
if ($options{'s'}) {
    my $size = $options{'s'};
    if ($size !~ /^(\d+$)$/ || int($1) < 0) {
	print STDERR "Invalid size '$size'\n";
	exit(1);
    }
    $minsize = int($1);
}

if (! -x "$BINDIR/rc/rc.storage" ||
    !open(SI, "sudo $BINDIR/rc/rc.storage info 2>&1 |")) {
    print STDERR "Could not get storageinfo\n";
    exit(1);
}

my $indi = 0;
my (@hdds, @ssds);

while (my $line = <SI>) {
    chomp($line);

    # only interested in DISKINFO section
    if ($line =~ /DISKINFO:/) {
	$indi = 1;
	next;
    }
    next if (!$indi);
    if ($line =~ /LOCAL_SNMAP:/) {
	$indi = 0;
	next;
    }
    my %args = ();
    $args{'type'} = "UNKNOWN";
    $args{'level'} = 99;
    $args{'inuse'} = 1;
    $args{'size'} = 0;
    foreach my $kv (split(/,/, $line)) {
	if ($kv =~ /^\s*([^=]+)=(\S*)$/) {
	    $args{$1} = $2;
	}
    }

    # We only care about whole disks (level==0) that are free (inuse==0)...
    if ($args{'type'} ne "DISK" || $args{'level'} > 0 || $args{'inuse'} != 0) {
	next;
    }
    # ...and it must be big enough
    my $size = int(int($args{'size'}) / 1024);
    if ($size < $minsize) {
	next;

    }
    if ($args{'disktype'} eq "HDD") {
	push(@hdds, $args{'name'});
    } elsif ($args{'disktype'} eq "SSD") {
	push(@ssds, $args{'name'});
    }
}
close(SI);

if ($docsv) {
    my @list = ();
    if ($dohdd) {
	push @list, @hdds;
    }
    if ($dossd) {
	push @list, @ssds;
    }
    if (@list > 0) {
	print join(',', sort @list), "\n";
    }
} else {
    if ($dohdd) {
	foreach my $hdd (sort @hdds) {
	    print "$hdd\n";
	}
    }
    if ($dossd) {
	foreach my $ssd (sort @ssds) {
	    print "$ssd\n";
	}
    }
}
exit(0);
