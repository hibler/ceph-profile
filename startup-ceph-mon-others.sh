#!/bin/bash -ex

WAITFOREVER=0
FORCE=0

# intervals to sleep while waiting for other nodes; 5 minutes total
SLEEPS="2 3 5 5 5 10 10 10 10 15 15 30 30 30 60 60"

function waitfor() {
    while true; do
	for w in $SLEEPS; do
	    if [ -e "$STASHDIR/$1" ]; then
		return 0
	    fi
	    echo "Waiting $w seconds for $STASHDIR/$1..."
	    sleep $w
	done
	if [ $WAITFOREVER -eq 0 ]; then
	    break
	fi
	SLEEPS="60 60 60 60 60 60 60 60 60 60"
    done

    return 1
}

# attempt to be idempotent
if [ $FORCE -eq 0 -a -e "/etc/ceph/ceph.conf" -a -e "/etc/ceph/ceph.mon.keyring" ]; then
    echo "MON nodes already configured, set FORCE=1 to force reconfig"
    exit 0
fi

/local/repository/ceph-common.sh

EXP_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $2}'`
PROJ_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $3}'`
STASHDIR=/proj/$PROJ_NAME/exp/$EXP_NAME/tmp

NUM_OSDS=`geni-get manifest | grep num_osds | sed "s/.*>\(.*\)<.*/\1/g"`

if ! waitfor "isready1"; then
    echo "WARNING: mon0 did not become ready after a long time"
fi

sudo cp -p $STASHDIR/ceph.client.admin.keyring \
     $STASHDIR/ceph.mon.keyring \
     $STASHDIR/initial-monmap \
     $STASHDIR/ceph.conf /etc/ceph/

MON_IDX=`cat /var/emulab/boot/nickname | sed "s/^ceph-mon\([0-9]\).*/\1/"`

sudo ceph-mon --mkfs -i ${MON_IDX} --monmap /etc/ceph/initial-monmap --keyring /etc/ceph/ceph.mon.keyring
sudo chown -R ceph:ceph /var/lib/ceph/mon

sudo systemctl enable ceph-mon@${MON_IDX}
sudo systemctl start ceph-mon@${MON_IDX}

# Create and start a mgr daemon
sudo mkdir -p /var/lib/ceph/mgr/ceph-${MON_IDX}
sudo sh -c "ceph auth get-or-create mgr.${MON_IDX} \
    mon 'allow profile mgr' osd 'allow *' mds 'allow *' \
    > /var/lib/ceph/mgr/ceph-${MON_IDX}/keyring"
sudo ceph-mgr -i ${MON_IDX}

# Allow access by admin user
sudo ceph auth caps client.admin \
    mon 'allow *' osd 'allow *' mds 'allow *' mgr 'allow *'

# Wait for mon0 to setup any filesystem
ISFS=0
if waitfor "isready2"; then
    if [ -e "$STASHDIR/withfs" ]; then
	ISFS=1
    fi
else
    echo "WARNING: mon0 did not finish filesystem activity, assuming none"
fi
if [ $ISFS ]; then
    sudo -u ceph mkdir -p /var/lib/ceph/mds/ceph-m${MON_IDX}
    sudo sh -c "ceph auth get-or-create mds.m${MON_IDX} mds 'allow' osd 'allow *' mon 'allow rwx' > /var/lib/ceph/mds/ceph-m${MON_IDX}/keyring"
    sudo ceph-mds --cluster ceph -i m${MON_IDX} -m ceph-mon${MON_IDX}-fe-lan:6789
    # started above, but enable it for future boots
    sudo systemctl enable ceph-mds@m${MON_IDX}
fi

exit 0
