#!/bin/bash -ex

# intervals to sleep while waiting for other nodes; 5 minutes total
SLEEPS="2 3 5 5 5 10 10 10 10 15 15 30 30 30 60 60"

function waitfor() {
    for w in $SLEEPS; do
	if [ -e "$STASHDIR/$1" ]; then
	    return 0
	fi
	echo "Waiting $w seconds for $STASHDIR/$1..."
	sleep $w
    done

    return 1
}

/local/repository/ceph-common.sh

EXP_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $2}'`
PROJ_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $3}'`
STASHDIR=/proj/$PROJ_NAME/exp/$EXP_NAME/tmp

if ! waitfor "isready2"; then
    echo "WARNING: mon0 did not become ready after a long time"
fi
WITHFS=0
if [ -e "$STASHDIR/withfs" ]; then
    WITHFS=1
fi

sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.conf /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.client.admin.keyring /etc/ceph/

if [ $WITHFS -ne 0 ]; then
    echo "Mounting CephFS on /cephfs"
    sudo mkdir -p /cephfs
    sudo mount -t ceph -o name=admin :/ /cephfs
fi

exit 0
