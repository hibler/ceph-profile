#!/bin/bash -ex

DEBUG=0
NOWAL=1
FORCE=0
WAITFOREVER=1

# intervals to sleep while waiting for other nodes; 5 minutes total
SLEEPS="2 3 5 5 5 10 10 10 10 15 15 30 30 30 60 60"

function waitfor() {
    while true; do
	for w in $SLEEPS; do
	    if [ -e "$STASHDIR/$1" ]; then
		return 0
	    fi
	    echo "Waiting $w seconds for $STASHDIR/$1..."
	    sleep $w
	done
	if [ $WAITFOREVER -eq 0 ]; then
	    break
	fi
	SLEEPS="60 60 60 60 60 60 60 60 60 60"
    done

    return 1
}

# Before anything else, drop some info for the mon0 setup
EXP_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $2}'`
PROJ_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $3}'`

STASHDIR=/proj/$PROJ_NAME/exp/$EXP_NAME/tmp
sudo mkdir -p $STASHDIR

# attempt to be idempotent
if [ $FORCE -eq 0 -a -e "/etc/ceph/ceph.conf" -a -e "/var/lib/ceph/bootstrap-osd/ceph.keyring" ]; then
    echo "OSDs already configured, set FORCE=1 to force reconfig"
    exit 0
fi

OSD_NODE_IDX=`cat /var/emulab/boot/nickname | sed "s/^ceph-osd\([0-9]\).*/\1/"`
MYOSDS=( $(sudo perl /local/repository/find_disks.pl) )
echo "${MYOSDS[*]}" > $STASHDIR/ceph-osd${OSD_NODE_IDX}-disks

if [ $DEBUG -eq 0 ]; then
    /local/repository/ceph-common.sh

    if ! waitfor "isready1"; then
	echo "WARNING: mon0 did not become ready after a long time"
    fi

    sudo cp -p $STASHDIR/ceph.client.admin.keyring \
	 $STASHDIR/ceph.mon.keyring \
	 $STASHDIR/initial-monmap \
	 $STASHDIR/ceph.conf /etc/ceph/

    sudo sh -c "ceph auth get client.bootstrap-osd > /var/lib/ceph/bootstrap-osd/ceph.keyring"
fi

# Extract OSD info that was stashed by mon setup
FIRST_OSD_IDX=`sed -n -e 's/IDX=//p' $STASHDIR/ceph-osd${OSD_NODE_IDX}-info`
OSDS=( `sed -n -e 's/OSDS=//p' $STASHDIR/ceph-osd${OSD_NODE_IDX}-info` )
sudo mv -f $STASHDIR/ceph-osd${OSD_NODE_IDX}-info $STASHDIR/ceph-osd${OSD_NODE_IDX}-info.old
OSDS_PER_NODE=${#OSDS[@]}
if [ $OSDS_PER_NODE -eq 0 ]; then
    echo "ceph-osd${OSD_NODE_IDX}: mon0 listed no drives, we did not come up in time."
    exit 1
fi

volumes=""
sizes=""

#
# XXX for the powder storage nodes we don't want to use mkextrafs,
# as it will create the VG on the janky BOSS controller M.2s.
# We need to create a software RAID1 on the two NVMe drives and create the
# filesystems there.
#
PHYS_HOST=`cat /var/emulab/boot/nodeid | grep powder-store`
if [ -n "$PHYS_HOST" -a -e "/dev/nvme0n1" -a -e "/dev/nvme1n1" ]; then
    echo "Creating VG on Powder storage nodes..."
    echo 'yes' | \
    sudo mdadm --create /dev/md/meta --level=1 --raid-devices=2 /dev/nvme[01]n1

    #
    # XXX make sure there is no leftover LVM state.
    # This should not be a problem normally, unless you just used this node
    # in a ceph install without going through a frisbee load of the node.
    #
    vgs=`sudo vgs --noheadings -o vgname | grep ceph`
    for vg in $vgs; do
	sudo vgremove -yf $vg
    done
    for osd in ${MYOSDS[*]}; do
	sudo pvremove -yf /dev/$osd
    done
    sudo pvremove /dev/md/meta >/dev/null 2>&1


    sudo pvcreate /dev/md/meta
    sudo vgcreate vg_ceph /dev/md/meta
    for i in $(seq 0 $((${OSDS_PER_NODE}-1)))
    do
	OSD_IDX=$((${FIRST_OSD_IDX}+$i))
	if [ $NOWAL -eq 0 ]; then
	    sudo lvcreate -L 50G -n ceph-osd${OSD_IDX}-db vg_ceph
	    sudo lvcreate -L 10G -n ceph-osd${OSD_IDX}-wal vg_ceph
	else
	    sudo lvcreate -L 100G -n ceph-osd${OSD_IDX}-db vg_ceph
	fi
    done
    
else
    for i in $(seq 0 $((${OSDS_PER_NODE}-1)))
    do
	OSD_IDX=$((${FIRST_OSD_IDX}+$i))
	echo ${OSDS[$i]} ${OSD_IDX}
	if [ $NOWAL -eq 0 ]; then
	    volumes+="ceph-osd${OSD_IDX}-db,ceph-osd${OSD_IDX}-wal,"
	    sizes+="10000,10000,"
	else
	    volumes+="ceph-osd${OSD_IDX}-db"
	    sizes+="10000,"
	fi
    done

    volumes=`echo "${volumes%,}"`
    sizes=`echo "${sizes%,}"`

    if [ $DEBUG -ne 0 ]; then
	echo "Would startup $OSDS_PER_NODE OSDS"
	exit 0
    fi

    sudo /usr/local/etc/emulab/mkextrafs.pl \
	 -v vg_ceph -m ${volumes} -z ${sizes} -l
fi

for i in $(seq 0 $((${OSDS_PER_NODE}-1)))
do
    osd_uuid=`uuidgen`
    OSD_IDX=$((${FIRST_OSD_IDX}+$i))
    BLOCK_DATA=/dev/${OSDS[$i]}
    BLOCK_DB=/dev/vg_ceph/ceph-osd${OSD_IDX}-db
    if [ $NOWAL -eq 0 ]; then
	BLOCK_WAL=/dev/vg_ceph/ceph-osd${OSD_IDX}-wal
    else
	BLOCK_WAL=
    fi

    #
    # XXX "ceph-volume raw create" does not seem to work with an LVM-based
    # db or wal device. So... we are just going to use the basic
    # "ceph-volume lvm create" call. This creates an LVM VG on every OSD
    # disk device, but the docs claim that this does not introduce any
    # significant overhead.
    #
    args="--bluestore --data $BLOCK_DATA --block.db $BLOCK_DB"
    if [ -n "$BLOCK_WAL" ]; then
	$args="$args --block.wal $BLOCK_WAL"
    fi
    sudo ceph-volume lvm create $args
done

exit 0
