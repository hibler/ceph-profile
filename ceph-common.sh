#!/bin/bash

if [ ! -x /usr/bin/ceph ]; then
    rel=$(lsb_release -sc)
    wget -qO-  https://download.ceph.com/keys/release.asc | \
	sudo tee /etc/apt/trusted.gpg.d/myrepo.asc

    sudo apt-add-repository -y "deb https://download.ceph.com/debian-quincy $rel main"
    sudo apt-get update
    sudo apt-get install -y ceph ceph-mds ceph-mgr-dashboard attr
else
    echo "Ceph already installed"
fi
