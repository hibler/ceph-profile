"""Ceph profile"""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab as emulab

# Emulab
emulab_info = {
    "d430": {
        "desc": "d430: 2x8-core 2.4GHz 64GB RAM",
        "disk": { "hdd": 2, "ssd": 1, "nvme": 0 },
        "netif": { "10g": 2, "25g": 0, "40g": 0, "100g": 0 }
    },
    "d430k": {
        "desc": "d430k: 1x10-core 2.4GHz 64GB RAM",
        "disk": { "hdd": 2, "ssd": 0, "nvme": 0 },
        "netif": { "10g": 1, "25g": 0, "40g": 0, "100g": 0 }
    },
    "d820": {
        "desc": "d820: 4x8-core 2.2GHz 128GB RAM",
        "disk": { "hdd": 6, "ssd": 0, "nvme": 0 },
        "netif": { "10g": 4, "25g": 0, "40g": 0, "100g": 0 }
    },
    "d740": {
        "desc": "Powder d740: 2x12-core 2.6GHz 96GB RAM",
        "disk": { "hdd": 0, "ssd": 2, "nvme": 0 },
        "netif": { "10g": 2, "25g": 0, "40g": 0, "100g": 0 }
    },
    "d840": {
        "desc": "Powder d840: 4x16-core 2.1GHz 768GB RAM",
        "disk": { "hdd": 0, "ssd": 1, "nvme": 4 },
        "netif": { "10g": 0, "25g": 0, "40g": 2, "100g": 0 }
    },
    "r7525s": {
        "desc": "Powder r7525s: 2x16-core 3.0GHz 256GB RAM",
        "disk": { "hdd": 12, "ssd": 0, "nvme": 2 },
        "netif": { "10g": 0, "25g": 2, "40g": 0, "100g": 0 }
    }
};

pc = portal.Context()

pc.defineParameter("noinit", "Allocate only",
                   portal.ParameterType.BOOLEAN, False,
                   longDescription="Allocate nodes but do not run startup scripts."
                  )
pc.defineParameter("sharedvlan", "Use shared vlan",
                   portal.ParameterType.STRING, "",
                   longDescription="If non-null, use the given shared vlan for the FE network."
                  )
pc.defineParameter("num_osd_nodes", "Number of OSD nodes",
                   portal.ParameterType.INTEGER, 1,
                   longDescription="Number of OSD nodes. Each node will have one or more OSDs based on the setting of 'osd_drives'."
                  )

pc.defineParameter("osd_drives",
                   "What devices to use for OSDs (comma separated, 'all' to use all available drives)",
                   portal.ParameterType.STRING, "all",
                   longDescription=
                   "Comma separated list of devices in /dev to use for OSDs on each node ('all' to use all available drives)."
                  )

pc.defineParameter("osd_hw_type", "Hardware Type for OSD nodes",
                   portal.ParameterType.STRING, "d430",
                   (
                    ("d430", emulab_info["d430"]["desc"]),
                    ("d820", emulab_info["d820"]["desc"]),
                    ("d840", emulab_info["d740"]["desc"]),
                    ("r7525s", emulab_info["r7525s"]["desc"]),
                   ),
                   longDescription="Ceph OSD node type."
                  )

pc.defineParameter("num_mon_nodes", "Number of Monitor nodes",
                   portal.ParameterType.INTEGER, 1,
                   longDescription="Number of monitor nodes."
                  )

pc.defineParameter("mon_hw_type", "Hardware Type for Monitor nodes",
                   portal.ParameterType.STRING, "d430",
                   (
                    ("d430", emulab_info["d430"]["desc"]),
                    ("d430k", emulab_info["d430k"]["desc"]),
                    ("d740", emulab_info["d740"]["desc"]),
                   ),
                   longDescription="Ceph monitor node type."
                  )

pc.defineParameter("num_client_nodes", "Number of client nodes",
                   portal.ParameterType.INTEGER, 0,
                   longDescription="Number of client nodes. Clients are normally attached via a shared-vlan."
                  )

pc.defineParameter("client_hw_type", "Hardware Type for client nodes",
                   portal.ParameterType.STRING, "d430",
                   (
                    ("d430", emulab_info["d430"]["desc"]),
                    ("d740", emulab_info["d740"]["desc"]),
                   ),
                   longDescription="Ceph client node type."
                  )


params = pc.bindParameters()

if (params.num_mon_nodes % 2) != 1:
    perr = portal.ParameterError("Must be an odd number of monitor nodes.",
                                 ['num_mon_nodes'])
    pc.reportError(perr)

pc.verifyParameters()

request = portal.context.makeRequestRSpec()

nodes_ceph_mon = []
nodes_ceph_osd = []
nodes_ceph_client = []

def get_ifaceinfo(hwtype):
    bw = 1
    mpx = 0
    nif = emulab_info[hwtype]["netif"]["40g"]
    if nif > 0:
        bw = 40
    else:
        nif = emulab_info[hwtype]["netif"]["25g"]
        if nif > 0:
            bw = 25
        else:
            nif = emulab_info[hwtype]["netif"]["10g"]
            if nif > 0:
                bw = 10
                pass
            pass
        pass
    if nif == 1:
        mpx = 1
        pass
    return bw, mpx

link_fe = request.LAN("fe-lan")
link_fe_idx = 1
# Use jumbo frames where possible
link_fe.setJumboFrames();
# Use shared vlan if specified
if params.sharedvlan != "":
    link_fe.connectSharedVlan(params.sharedvlan)

# If there are multiple OSDs, use a distinct backend network
if params.num_osd_nodes > 1:
    link_be = request.LAN("be-lan")
    link_be_idx = 1
    # Use jumbo frames where possible
    link_be.setJumboFrames();

# mon
for i in range(params.num_mon_nodes):
    node_ceph_mon = request.RawPC("ceph-mon{0}".format(i))
    if i == 0:
        command_template = "/local/repository/startup-ceph-mon.sh {0}"
    else:
        command_template = "/local/repository/startup-ceph-mon-others.sh {0}"
    command = command_template.format(params.osd_drives)
    if params.noinit == False:
        node_ceph_mon.addService(rspec.Execute(shell="bash", command=command))
    node = node_ceph_mon

    #
    # XXX hardwired for now--startup script might make assumptions about the
    # version of Ceph (quincy).
    # 
    node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"

    # External network for ceph
    iface_fe = node.addInterface("if3")
    iface_fe.addAddress(rspec.IPv4Address("192.168.152.%d"%(link_fe_idx),
                                         "255.255.248.0"))
    link_fe_idx += 1

    bw, mpx = get_ifaceinfo(params.mon_hw_type)
    iface_fe.bandwidth = bw*1000*1000
    if mpx != 0:
        iface_fe.link_multiplexing = True
        iface_fe.vlan_tagging = True
        pass
    link_fe.addInterface(iface_fe)

    # XXX hack for Powder ceph cluster
    # allocate in order for mike's sanity
    if params.mon_hw_type == "d430k":
        node_ceph_mon.component_id = "kn" + str(i+1)
    else:
        node_ceph_mon.hardware_type = params.mon_hw_type
    nodes_ceph_mon.append(node_ceph_mon)

# osd
for i in range(params.num_osd_nodes):
    node_ceph_osd = request.RawPC("ceph-osd{0}".format(i))
    command_template = "/local/repository/startup-ceph-osd.sh {0}"
    command = command_template.format(params.osd_drives)
    if params.noinit == False:
        node_ceph_osd.addService(rspec.Execute(shell="bash", command=command))
    # XXX hack for Powder ceph cluster
    # allocate in order for mike's sanity
    if params.osd_hw_type == "r7525s":
        node_ceph_osd.component_id = "powder-store" + str(i + 1)
    else:
        node_ceph_osd.hardware_type = params.osd_hw_type
    nodes_ceph_osd.append(node_ceph_osd)
    node = node_ceph_osd

    # XXX hardwired for now
    if params.noinit == True:
        node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
    else:
        node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU20-64-STD"

    # External network for ceph
    iface_fe = node.addInterface("if3")
    iface_fe.addAddress(rspec.IPv4Address("192.168.152.%d"%(link_fe_idx),
                                         "255.255.248.0"))

    # XXX hack for Powder ceph cluster
    # make sure we use the fort-side interface for the FE
    if params.osd_hw_type == "r7525s":
        iface_fe.component_id = "eth0"

    link_fe_idx += 1

    bw, mpx = get_ifaceinfo(params.osd_hw_type)
    iface_fe.bandwidth = bw*1000*1000
    if mpx != 0 and params.num_osd_nodes > 1:
        iface_fe.link_multiplexing = True
        iface_fe.vlan_tagging = True
        pass
    link_fe.addInterface(iface_fe)

    # Internal network for ceph
    if params.num_osd_nodes > 1:
        iface_be = node.addInterface("if4")
        iface_be.addAddress(rspec.IPv4Address("192.168.4.%d"%(link_be_idx),
                                              "255.255.255.0"))
        link_be_idx += 1

        iface_be.bandwidth = bw*1000*1000
        if mpx != 0:
            iface_be.link_multiplexing = True
            iface_be.vlan_tagging = True
            pass
	link_be.addInterface(iface_be)

# clients
for i in range(params.num_client_nodes):
    node_ceph_client = request.RawPC("ceph-client{0}".format(i))
    command="/local/repository/ceph-common.sh"
    if params.noinit == False:
        node_ceph_client.addService(rspec.Execute(shell="bash", command=command))
    node = node_ceph_client

    # XXX hardwired for now
    if params.noinit == True:
        node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
    else:
        node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU20-64-STD"

    # External network for ceph
    iface_fe = node.addInterface("if3")
    iface_fe.addAddress(rspec.IPv4Address("192.168.152.%d"%(link_fe_idx),
                                          "255.255.248.0"))
    link_fe_idx += 1

    bw, mpx = get_ifaceinfo(params.client_hw_type)
    iface_fe.bandwidth = bw*1000*1000
    if mpx != 0:
        iface_fe.link_multiplexing = True
        iface_fe.vlan_tagging = True
        pass
    link_fe.addInterface(iface_fe)

    node_ceph_client.hardware_type = params.client_hw_type
    nodes_ceph_client.append(node_ceph_client)

all_nodes = nodes_ceph_mon + nodes_ceph_osd + nodes_ceph_client


portal.context.printRequestRSpec()
