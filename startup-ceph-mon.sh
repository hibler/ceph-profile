#!/bin/bash -ex

DEBUG=0
WITHFS=1
WITHDASHBOARD=0
WAITFOREVER=0
FORCE=0

# intervals to sleep while waiting for other nodes; 5 minutes total
SLEEPS="2 3 5 5 5 10 10 10 10 15 15 30 30 30 60 60"

function waitfor() {
    while true; do
	for w in $SLEEPS; do
	    if [ -e "$STASHDIR/$1" ]; then
		return 0
	    fi
	    echo "Waiting $w seconds for $STASHDIR/$1..."
	    sleep $w
	done
	if [ $WAITFOREVER -eq 0 ]; then
	    break
	fi
	SLEEPS="60 60 60 60 60 60 60 60 60 60"
    done

    return 1
}

# attempt to be idempotent
if [ $FORCE -eq 0 -a -e "/etc/ceph/ceph.conf" -a -e "/etc/ceph/ceph.mon.keyring" ]; then
    echo "MON nodes already configured, set FORCE=1 to force reconfig"
    exit 0
fi

if [ -z "$1" ]; then
    echo "Usage: $0 <disk list>"
    echo "       where <disk list> is either \"all\" or"
    echo "       a space-separated list of disks; e.g., \"sdb sdc sdd\""
    exit 1
fi

if [ $DEBUG -eq 0 ]; then
    /local/repository/ceph-common.sh
fi

geni-get manifest > /tmp/manifest
NUM_OSD_NODES=`grep num_osd_nodes /tmp/manifest | sed "s/.*>\(.*\)<.*/\1/g"`
NUM_MON_NODES=`grep num_mon_nodes /tmp/manifest | sed "s/.*>\(.*\)<.*/\1/g"`

EXP_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $2}'`
PROJ_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $3}'`
STASHDIR=/proj/$PROJ_NAME/exp/$EXP_NAME/tmp
sudo mkdir -m 770 -p $STASHDIR
sudo rm -rf $STASHDIR/isready* $STASHDIR/withfs $STASHDIR/isready2

# Generate a UUID for the cluster
CUUID=`uuidgen`

if [ $DEBUG -eq 0 ]; then
    # monitor keyring
    sudo ceph-authtool --create-keyring /etc/ceph/ceph.mon.keyring \
	--gen-key -n mon. --cap mon 'allow *'
    # admin keyring
    sudo ceph-authtool --create-keyring /etc/ceph/ceph.client.admin.keyring \
	--gen-key -n client.admin --cap mon 'allow *' \
	--cap osd 'allow *' --cap mds 'allow *'
    # boostrap-osd keyring
    sudo ceph-authtool --create-keyring /var/lib/ceph/bootstrap-osd/ceph.keyring \
	--gen-key -n client.bootstrap-osd --cap mon 'profile bootstrap-osd' \
	--cap mgr 'allow r'
    # add admin and boostrap keys to mon keyring
    sudo ceph-authtool /etc/ceph/ceph.mon.keyring \
	--import-keyring /etc/ceph/ceph.client.admin.keyring
    sudo ceph-authtool /etc/ceph/ceph.mon.keyring \
	--import-keyring /var/lib/ceph/bootstrap-osd/ceph.keyring
    sudo chown ceph:ceph /etc/ceph/ceph.mon.keyring
fi

addstr=""
hosts=()
addrs=()
memstr=""
hoststr=""
for i in $(seq 0 $((${NUM_MON_NODES}-1))); do
    ADDR=`grep ceph-mon${i}-fe-lan /etc/hosts | cut -f 1`
    addstr="$addstr --add $i $ADDR"
    hosts[$i]="ceph-mon-$i"
    addrs[$i]=$ADDR
    if [ -z "$memstr" ]; then
	memstr=$i
    else
	memstr="$memstr, $i"
    fi
    hoststr="$hoststr [v2:ceph-mon-${i}:3300,v1:ceph-mon-${i}:6789]"
done
if [ $DEBUG -eq 0 ]; then
    sudo monmaptool --create --fsid $CUUID $addstr /etc/ceph/initial-monmap
fi

cat > /tmp/ceph.conf <<END
[global]
  fsid = $CUUID
  cluster = ceph
  public network = 192.168.152.0/21
  cluster network = 192.168.4.0/24

  auth cluster required = cephx
  auth service required = cephx
  auth client required = cephx
  auth_allow_insecure_global_id_reclaim = false

  osd pool default size = 2
  osd pool default min size = 1
  # (100 * #osds) / pool_default_size
  osd_pool_default_pg_num = 256

[mon]
  mon host = $hoststr
  mon addr = ${addrs[@]}
  mon initial members = $memstr

END

for i in $(seq 0 $((${NUM_MON_NODES}-1))); do
    cat >> /tmp/ceph.conf <<END
[mon.$i]
  host = ${hosts[$i]}
  mon addr = ${addrs[$i]}:6789

END
done

# Setup OSDs
cat >> /tmp/ceph.conf <<END
[osd]
  osd journal size = 1024
  filestore xattr use omap = true
  osd mkfs type = ext4
  osd mount options ext4 = user_xattr,rw,noatime
  osd max object name len = 256
  osd max object namespace len = 64

END

IFS="," read -r -a OSDS <<< $1
echo "Num OSDs: ${#OSDS[@]}"

OSD_IDX=0
for i in $(seq 0 $((${NUM_OSD_NODES}-1)))
do
    if [ "${OSDS[0]}" = "all" ]; then
	MYOSDS=()
	#
	# XXX how long is long enough to wait? Storage nodes of exotic types
	# could take a long time to allocate and potentially reload the OS on.
	#
	if waitfor "ceph-osd$i-disks"; then
	    MYOSDS=( `cat $STASHDIR/ceph-osd$i-disks` )
	    sudo mv -f $STASHDIR/ceph-osd$i-disks $STASHDIR/ceph-osd$i-disks.old
	fi
    else
	MYOSDS=$OSDS
    fi
    NUM_OSDS=${#MYOSDS[@]}
    if [ $NUM_OSDS -eq 0 ]; then
	echo "`date` ceph-osd${i}: Could not determine local drives, skipped..."
    fi
    echo "IDX=$OSD_IDX" > /tmp/ceph-info
    echo "OSDS=${MYOSDS[*]}" >> /tmp/ceph-info
    sudo cp /tmp/ceph-info $STASHDIR/ceph-osd$i-info
    rm -f /tmp/ceph-info

    OSD_FE=`grep ceph-osd$i-fe-lan /etc/hosts | cut -f 1`
    OSD_BE=`grep ceph-osd$i-be-lan /etc/hosts | cut -f 1`

    for j in $(seq 0 $((${NUM_OSDS}-1)))
    do
        BLOCK_DATA=/dev/${MYOSDS[$j]}
cat >> /tmp/ceph.conf <<END
[osd.${OSD_IDX}]
  host = ceph-osd-$i
  devs = ${BLOCK_DATA}
  public addr = ${OSD_FE}
  cluster addr = ${OSD_BE}

END
        OSD_IDX=$((${OSD_IDX}+1))
    done
done
TOTAL_OSDS=$OSD_IDX

if [ $WITHFS -ne 0 ]; then
    for i in $(seq 0 $((${NUM_MON_NODES}-1))); do
	cat >> /tmp/ceph.conf <<END
[mds.m$i]
  host = ceph-mon-$i

END
    done
fi

if [ $DEBUG -ne 0 ]; then
    echo "Wrote /tmp/ceph.conf"
    exit 0
fi

sudo chown ceph:ceph /tmp/ceph.conf
sudo mv /tmp/ceph.conf /etc/ceph/ceph.conf

sudo ceph-mon --mkfs -i 0 \
    --monmap /etc/ceph/initial-monmap --keyring /etc/ceph/ceph.mon.keyring
sudo chown -R ceph:ceph /var/lib/ceph/mon

sudo cp -p \
     /etc/ceph/ceph.client.admin.keyring \
     /etc/ceph/ceph.mon.keyring \
     /etc/ceph/initial-monmap \
     /etc/ceph/ceph.conf $STASHDIR/

#
# Other mon nodes and osd nodes will be waiting for the "isready" flag.
# Create that file now so that they can continue.
#
sudo touch $STASHDIR/isready1

sudo systemctl enable ceph-mon@0
sudo systemctl start ceph-mon@0

# XXX this should not be needed but is
sudo ceph mon enable-msgr2

# Create and start a mgr daemon
sudo mkdir -p /var/lib/ceph/mgr/ceph-0
sudo sh -c "ceph auth get-or-create mgr.0 \
    mon 'allow profile mgr' osd 'allow *' mds 'allow *' \
    > /var/lib/ceph/mgr/ceph-0/keyring"
sudo ceph-mgr -i 0
# started above, but enable it for future boots
sudo systemctl enable ceph-mgr@0

# Enable the dashboard on mon0
if [ $WITHDASHBOARD -ne 0 ]; then
    sudo ceph mgr module enable dashboard
    sudo ceph dashboard create-self-signed-cert
    echo "FuBar" | sudo dd of=/etc/ceph/dashboard.pswd
    chmod 600 /etc/ceph/dashboard.pswd
    sudo ceph dashboard ac-user-create elabman -i /etc/ceph/dashboard.pswd administrator
fi

# Allow access by admin user
sudo ceph auth caps client.admin \
    mon 'allow *' osd 'allow *' mds 'allow *' mgr 'allow *'

# Fire up a metadata server if using an FS
if [ $WITHFS -ne 0 ]; then
    sudo -u ceph mkdir -p /var/lib/ceph/mds/ceph-m0
    sudo sh -c "ceph auth get-or-create mds.m0 mds 'allow' osd 'allow *' mon 'allow rwx' > /var/lib/ceph/mds/ceph-m0/keyring"
    sudo ceph-mds --cluster ceph -i m0 -m ceph-mon0-fe-lan:6789
    # started above, but enable it for future boots
    sudo systemctl enable ceph-mds@m0
fi

#
# XXX Wait for OSDs to come ready.
# "ceph status | grep osd:" shows:
#    osd: 72 osds: 72 up (since 25h), 72 in (since 25h)
#
# TOTAL_OSDS is the number we expect to see and be "up" and "in".
#
FOUND_OSDS=0
while [ $TOTAL_OSDS -ne $FOUND_OSDS ]; do
    sleep 5
    FOUND_OSDS=`sudo ceph osd stat | sed -e 's/^\([0-9][0-9]*\) osds: .*/\1/'`
    echo "Found $FOUND_OSDS of $TOTAL_OSDS OSDs..."
done

#
# Creating pools.
# For now we just let it autoscale the placement groups.
#
sudo ceph config set global osd_pool_default_pg_autoscale_mode on

if [ $WITHFS -ne 0 ]; then
    sudo ceph osd pool create cephfs_ddata
    sudo ceph osd pool create cephfs_metadata 
    sudo ceph fs new cephfs cephfs_metadata cephfs_ddata
    sudo mkdir -p /cephfs
    # mount the FS--apparently you have to wait a bit after the creation
    sleep 5
    sudo mount -t ceph -o name=admin :/ /cephfs
    if [ $? -ne 0 ]; then
	sleep 30
	sudo mount -t ceph -o name=admin :/ /cephfs
    fi
fi

#
# For the Powder cluster we have 6 machines with 12 disks (OSDs) each.
# We will erasure code 4+2 with a host level CRUSH failure domain. This
# ensure that no more than one piece of any object will be on the same host.
#
if [ $TOTAL_OSDS -eq 72 ]; then
    sudo ceph osd erasure-code-profile set powder-profile \
	 k=4 \
	 m=2 \
	 crush-failure-domain=host
    sudo ceph osd pool create cephfs_data erasure powder-profile 
    sudo ceph osd pool set cephfs_data allow_ec_overwrites true
    if [ $WITHFS -ne 0 ]; then
	sudo ceph fs add_data_pool cephfs cephfs_data
	sudo setfattr -n ceph.dir.layout.pool -v cephfs_data /cephfs
    fi
fi

# Generate a minimal client config file
# XXX this part is still under development
#sudo sh -c "ceph config generate-minimal-conf > $STASHDIR/client-ceph.conf"
#sudo ceph auth get-or-create client.cephfs mon 'allow r' osd 'allow rwx pool=cephfs' mds 'allow rw' -o $STASHDIR/client.cephfs.keyring

# note to clients that things are ready to use
if [ $WITHFS -ne 0 ]; then
    sudo touch $STASHDIR/withfs
fi
sudo touch $STASHDIR/isready2

#
# Notes:
#
# 1. Destroying a filesystem (https://docs.ceph.com/en/latest/cephfs/administration/#taking-the-cluster-down-rapidly-for-deletion-or-disaster-recovery)
#
#   ceph fs fail root_ceph
#   ceph fs rm root_ceph --yes-i-really-mean-it
#
# 2. Take it all the way down to the studs:
#
#   ceph tell mon.* injectargs --mon_allow_pool_delete true
#   ceph osd pool delete cephfs_metadata cephfs_metadata \
#      --yes-i-really-really-mean-it --yes-i-really-really-mean-it-not-faking
#   ceph osd pool delete cephfs_data cephfs_data \
#      --yes-i-really-really-mean-it --yes-i-really-really-mean-it-not-faking
#
# 3. Including taking out osds:
#
#   osd=71
#   ceph osd out $osd
#   # login to osd host and
#   systemctl stop ceph-osd@$osd
#   ceph osd purge $osd --yes-i-really-mean-it
#   lvremove vg_ceph/ceph-osd${osd}-wal
#   lvremove vg_ceph/ceph-osd${osd}-db
#   rm -rf /var/lib/ceph/osd/ceph-$osd
#   # remove it from *all* /etc/ceph.conf files if it isn't coming back
#
# 4. Configuring multiple active MDSes:
#
#   sudo ceph fs set root_ceph max_mds <N>
#
# The manual recommends leaving at least one standby.
