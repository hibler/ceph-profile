"""Ceph profile"""

import geni.portal as portal
import geni.rspec.pg as rspec

# Wisconsin
wisc_info = {
    "c220g1": {
        "desc": "c220g1: 2x8-core 2.4GHz 128GB RAM",
        "disk": { "hdd": 2, "ssd": 1, "nvme": 0 },
        "netif": { "10g": 2, "25g": 0, "100g": 0 }
    },
    "c240g1": {
        "desc": "c240g1: 2x8-core 2.4GHz 128GB RAM",
        "disk": { "hdd": 12, "ssd": 2, "nvme": 0 },
        "netif": { "10g": 2, "25g": 0, "100g": 0 }
    },
    "c220g2": {
        "desc": "c220g2: 2x10-core 2.6GHz 160GB RAM",
        "disk": { "hdd": 2, "ssd": 1, "nvme": 0 },
        "netif": { "10g": 2, "25g": 0, "100g": 0 }
    },
    "c240g2": {
        "desc": "c240g2: 2x10-core 2.6GHz 160GB RAM",
        "disk": { "hdd": 6, "ssd": 2, "nvme": 0 },
        "netif": { "10g": 2, "25g": 0, "100g": 0 }
    }
};

pc = portal.Context()

pc.defineParameter("num_osd_nodes", "Number of OSDs",
                   portal.ParameterType.INTEGER, 1,
                   longDescription="Number of OSD nodes"
                  )

pc.defineParameter("osd_drives",
                   "What devices to use for OSDs (comma separated)",
                   portal.ParameterType.STRING, "sdb",
                   longDescription=
                   "Comma separated list of device fils to use for OSDs"
                  )

pc.defineParameter("osd_hw_type", "Hardware Type for OSDs",
                   portal.ParameterType.STRING, "c220g1",
                   (
                    ("c220g1", wisc_info["c220g1"]["desc"]),
                    ("c240g1", wisc_info["c240g1"]["desc"]),
                    ("c220g2", wisc_info["c220g2"]["desc"]),
                    ("c240g2", wisc_info["c240g2"]["desc"]),
                   ),
                   longDescription="Ceph OSD node type"
                  )

pc.defineParameter("num_mon_nodes", "Number of Monitors",
                   portal.ParameterType.INTEGER, 1,
                   longDescription="Number of monitor nodes"
                  )

pc.defineParameter("mon_hw_type", "Hardware Type for Monitors",
                   portal.ParameterType.STRING, "c220g1",
                   (
                    ("c220g1", wisc_info["c220g1"]["desc"]),
                    ("c220g2", wisc_info["c220g2"]["desc"]),
                   ),
                   longDescription="Ceph monitor node type"
                  )

pc.defineParameter("num_client_nodes", "Number of clients",
                   portal.ParameterType.INTEGER, 1,
                   longDescription="Number of client nodes"
                  )

pc.defineParameter("client_hw_type", "Hardware Type for clients",
                   portal.ParameterType.STRING, "c220g1",
                   (
                    ("c220g1", wisc_info["c220g1"]["desc"]),
                    ("c220g2", wisc_info["c220g2"]["desc"]),
                   ),
                   longDescription="Ceph client node type"
                  )


params = pc.bindParameters()

if (params.num_mon_nodes % 2) != 1:
    perr = portal.ParameterError("Must be an odd number of monitor nodes.",
                                 ['num_mon_nodes'])
    pc.reportError(perr)

pc.verifyParameters()

request = portal.context.makeRequestRSpec()

nodes_ceph_mon = []
nodes_ceph_osd = []
nodes_ceph_client = []

link_fe = request.LAN("fe-lan")
link_fe_idx = 1
if params.num_osd_nodes > 1:
    link_be = request.LAN("be-lan")
    link_be_idx = 1

# mon
for i in range(params.num_mon_nodes):
    node_ceph_mon = request.RawPC("ceph-mon{0}".format(i))
    if i == 0:
        command_template = "/local/repository/startup-ceph-mon.sh {0}"
    else:
        command_template = "/local/repository/startup-ceph-mon-others.sh {0}"
    command = command_template.format(params.osd_drives)
    node_ceph_mon.addService(rspec.Execute(shell="bash", command=command))
    node = node_ceph_mon

    # XXX hardwired for now
    node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU20-64-STD"

    # External network for ceph
    iface_fe = node.addInterface("if3")
    iface_fe.addAddress(rspec.IPv4Address("192.168.3.%d"%(link_fe_idx),
                                         "255.255.255.0"))
    link_fe_idx += 1

    ten_g = wisc_info[params.mon_hw_type]["netif"]["10g"]
    if ten_g > 0:
        iface_fe.bandwidth = 10*1000*1000
        if ten_g == 1:
            iface_fe.link_multiplexing = True
            iface_fe.vlan_tagging = True
            pass
        pass
    else:
        iface_fe.bandwidth = 1000*1000

    link_fe.addInterface(iface_fe)

    node_ceph_mon.hardware_type = params.mon_hw_type
    nodes_ceph_mon.append(node_ceph_mon)

# osd
for i in range(params.num_osd_nodes):
    node_ceph_osd = request.RawPC("ceph-osd{0}".format(i))
    command_template = "/local/repository/startup-ceph-osd.sh {0}"
    command = command_template.format(params.osd_drives)
    node_ceph_osd.addService(rspec.Execute(shell="bash", command=command))
    node_ceph_osd.hardware_type = params.osd_hw_type
    nodes_ceph_osd.append(node_ceph_osd)
    node = node_ceph_osd

    # XXX hardwired for now
    node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU20-64-STD"

    # External network for ceph
    iface_fe = node.addInterface("if3")
    iface_fe.addAddress(rspec.IPv4Address("192.168.3.%d"%(link_fe_idx),
                                         "255.255.255.0"))
    link_fe_idx += 1

    ten_g = wisc_info[params.osd_hw_type]["netif"]["10g"]
    ten_g_mpx = 0
    if ten_g > 0:
        iface_fe.bandwidth = 10*1000*1000
        if ten_g == 1:
            iface_fe.link_multiplexing = True
            iface_fe.vlan_tagging = True
            ten_g_mpx = 1
            pass
        pass
    else:
        iface_fe.bandwidth = 1000*1000

    link_fe.addInterface(iface_fe)

    # Internal network for ceph
    if params.num_osd_nodes > 1:
        iface_be = node.addInterface("if4")
        iface_be.addAddress(rspec.IPv4Address("192.168.4.%d"%(link_be_idx),
                                              "255.255.255.0"))
        link_be_idx += 1

        if ten_g > 0:
            iface_be.bandwidth = 10*1000*1000
            if ten_g_mpx:
                iface_be.link_multiplexing = True
                iface_be.vlan_tagging = True
                pass
            pass
        else:
            iface_be.bandwidth = 1000*1000

	link_be.addInterface(iface_be)


for i in range(params.num_client_nodes):
    node_ceph_client = request.RawPC("ceph-client{0}".format(i))
    command="/local/repository/ceph-common.sh"
    node_ceph_client.addService(rspec.Execute(shell="bash", command=command))
    node = node_ceph_client

    # XXX hardwired for now
    node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU20-64-STD"

    # External network for ceph
    iface_fe = node.addInterface("if3")
    iface_fe.addAddress(rspec.IPv4Address("192.168.3.%d"%(link_fe_idx),
                                          "255.255.255.0"))
    link_fe_idx += 1

    ten_g = wisc_info[params.mon_hw_type]["netif"]["10g"]
    if ten_g > 0:
        iface_fe.bandwidth = 10*1000*1000
        if ten_g == 1:
            iface_fe.link_multiplexing = True
            iface_fe.vlan_tagging = True
            pass
        pass
    else:
        iface_fe.bandwidth = 1000*1000

    link_fe.addInterface(iface_fe)

    node_ceph_client.hardware_type = params.client_hw_type
    nodes_ceph_client.append(node_ceph_client)

all_nodes = nodes_ceph_mon + nodes_ceph_osd + nodes_ceph_client


portal.context.printRequestRSpec()
